mod chapter;
mod lazy;

use chapter::Chapter;
use lazy::LazyImage;
use speedy2d::{
    color::Color,
    dimen::Vector2,
    font::{Font, TextAlignment, TextLayout, TextOptions},
    shape::Rectangle,
    window::{
        KeyScancode as KeyScan, MouseButton, VirtualKeyCode as KeyCode, WindowHandler, WindowHelper,
    },
    Graphics2D, Window,
};
use std::{
    fs::File,
    time::{Duration, Instant},
};

fn main() {
    const WIN_W: u32 = 800;
    const WIN_H: u32 = 600;
    let window = Window::new_centered("Intermezzo", (WIN_W, WIN_H)).unwrap();
    window.run_loop(State {
        window_w: WIN_W,
        window_h: WIN_H,
        mouse_x: 0.,
        mouse_y: 0.,
        scene: Scene::SplashScreen,
        scene_start_time: Instant::now(),
        chapter: None,
        chapter_amt: 1,
        image_bg: LazyImage::Vacant,
        image_panel: LazyImage::Filename("panel.png".to_owned()),
        font: Font::new(include_bytes!("../assets/font-main.ttf")).unwrap(),
    })
}

struct State {
    window_w: u32,
    window_h: u32,
    mouse_x: f32,
    mouse_y: f32,
    scene: Scene,
    scene_start_time: Instant,
    chapter: Option<Chapter>,
    chapter_amt: usize,
    image_bg: LazyImage,
    image_panel: LazyImage,
    font: Font,
}
#[derive(Clone, Copy, Debug)]
enum Scene {
    SplashScreen,
    MainMenu(Option<usize>),
    ChapterIntro(usize),
    ParagraphFadeIn(usize, usize),
    LineAppearing(usize, usize, usize, usize),
    LineStatic(usize, usize, usize, usize),
    ParagraphFadeOut(usize, usize),
}
impl State {
    /// Switch to another [`Scene`] and load necessary resources.
    fn switch_scene(&mut self, scene: Scene) {
        match scene {
            Scene::ChapterIntro(i) => {
                let filename = format!("chapters/{}.txt", i);
                self.chapter = Some(Chapter::from(File::open(filename).unwrap()));
            }
            Scene::ParagraphFadeIn(_, j) => {
                let filename = &self.chapter.as_ref().unwrap().paragraphs[j].bg_filename;
                self.image_bg = LazyImage::Filename(filename.to_owned());
            }
            _ => {}
        }
        self.scene = scene;
        self.scene_start_time = Instant::now();
    }
    /// Think of it as `switch_scene` to a certain default next [`Scene`].
    fn next_scene(&mut self) {
        self.switch_scene(match self.scene {
            Scene::SplashScreen => Scene::MainMenu(None),
            Scene::ChapterIntro(i) => Scene::ParagraphFadeIn(i, 0),
            Scene::ParagraphFadeIn(i, j) => Scene::LineAppearing(i, j, 0, 0),
            Scene::LineAppearing(i, j, k, stop_idx) => Scene::LineStatic(i, j, k, stop_idx),
            Scene::LineStatic(i, j, k, stop_idx) => {
                let lines = &self.chapter.as_ref().unwrap().paragraphs[j].lines;
                if stop_idx + 1 < lines[k].text_stops.len() {
                    Scene::LineAppearing(i, j, k, stop_idx + 1)
                } else if k + 1 < self.chapter.as_ref().unwrap().paragraphs[j].lines.len() {
                    Scene::LineAppearing(i, j, k + 1, 0)
                } else {
                    Scene::ParagraphFadeOut(i, j)
                }
            }
            Scene::ParagraphFadeOut(i, j) => {
                if j + 1 < self.chapter.as_ref().unwrap().paragraphs.len() {
                    Scene::ParagraphFadeIn(i, j + 1)
                } else if i + 1 < self.chapter_amt {
                    Scene::ChapterIntro(i + 1)
                } else {
                    Scene::MainMenu(None)
                }
            }
            _ => unimplemented!("there is no default next scene for {:?}", self.scene),
        });
    }
    /// May or may not perform a `switch_scene` to a certain default previous [`Scene`].
    fn prev_scene(&mut self) {
        match self.scene {
            Scene::LineAppearing(i, j, k, _) | Scene::LineStatic(i, j, k, _) => {
                if k > 0 {
                    let last_stop_idx = self.chapter.as_ref().unwrap().paragraphs[j].lines[k - 1]
                        .text_stops
                        .len()
                        - 1;
                    self.switch_scene(Scene::LineStatic(i, j, k - 1, last_stop_idx));
                }
            }
            _ => unimplemented!("there is no default previous scene for {:?}", self.scene),
        }
    }
}

impl WindowHandler for State {
    fn on_draw(&mut self, helper: &mut WindowHelper, graphics: &mut Graphics2D) {
        let scene_duration = Instant::now() - self.scene_start_time;
        let screen_rect =
            Rectangle::from_tuples((0., 0.), (self.window_w as f32, self.window_h as f32));
        self.image_bg.load_if_filename(graphics);
        self.image_panel.load_if_filename(graphics);
        graphics.clear_screen(Color::BLACK);
        match self.scene {
            Scene::SplashScreen => {
                const DUR: Duration = Duration::from_secs(3);
                let opacity = (scene_duration.as_secs_f32() / DUR.as_secs_f32()).clamp(0., 1.);
                let block = self.font.layout_text("Intermezzo", 48., TextOptions::new());
                let (w, h) = (block.width(), block.height());
                let text_pos = (
                    (self.window_w as f32 - w) / 2.,
                    (self.window_h as f32 - h) / 2.,
                );
                graphics.draw_text(text_pos, Color::from_rgba(1., 1., 1., opacity), &block);
                if scene_duration >= DUR {
                    self.next_scene();
                }
            }
            Scene::MainMenu(_) => {
                const LABELS: [&str; 4] = [
                    "Почати читання",
                    "Обрати розділ",
                    "Налаштування",
                    "Інформація",
                ];
                let mut blocks = vec![];
                for label in LABELS {
                    blocks.push(self.font.layout_text(label, 56., TextOptions::new()));
                }
                for (i, block) in blocks.iter().enumerate() {
                    let text_pos = (
                        (self.window_w as f32 - block.width()) / 2.,
                        self.window_h as f32 * 0.3 + i as f32 * (block.height() + 10.),
                    );
                    let text_col = if text_pos.0 <= self.mouse_x
                        && self.mouse_x <= text_pos.0 + block.width()
                        && text_pos.1 <= self.mouse_y
                        && self.mouse_y <= text_pos.1 + block.height()
                    {
                        self.scene = Scene::MainMenu(Some(i));
                        Color::WHITE
                    } else {
                        Color::LIGHT_GRAY
                    };
                    graphics.draw_text(text_pos, text_col, block);
                }
            }
            Scene::ChapterIntro(i) => {
                const DUR: Duration = Duration::from_secs(3);
                let title = format!("Chapter {}", i);
                let block = self.font.layout_text(&title, 64., TextOptions::new());
                let (w, h) = (block.width(), block.height());
                let text_pos = (
                    (self.window_w as f32 - w) / 2.,
                    (self.window_h as f32 - h) / 2.,
                );
                graphics.draw_text(text_pos, Color::WHITE, &block);
                if scene_duration >= DUR {
                    self.next_scene();
                }
            }
            Scene::ParagraphFadeIn(..) | Scene::ParagraphFadeOut(..) => {
                const DUR: Duration = Duration::from_secs(2);
                graphics.draw_rectangle_image(screen_rect.clone(), self.image_bg.unwrap());
                let mut opacity = (scene_duration.as_secs_f32() / DUR.as_secs_f32()).clamp(0., 1.);
                if let Scene::ParagraphFadeIn(..) = self.scene {
                    opacity = 1. - opacity;
                }
                graphics.draw_rectangle(screen_rect.clone(), Color::from_rgba(0., 0., 0., opacity));
                if scene_duration >= DUR {
                    self.next_scene();
                }
            }
            Scene::LineAppearing(_, j, k, stop_idx) | Scene::LineStatic(_, j, k, stop_idx) => {
                graphics.draw_rectangle_image(screen_rect.clone(), self.image_bg.unwrap());
                let mut line_just_stopped = false;
                let text_stops = &self.chapter.as_ref().unwrap().paragraphs[j].lines[k].text_stops;
                if let Some(text) = &self.chapter.as_ref().unwrap().paragraphs[j].lines[k].text {
                    let panel = self.image_panel.unwrap();
                    let panel_pos = (
                        (self.window_w as f32 - panel.size().x as f32) / 2.,
                        self.window_h as f32 * 0.8,
                    );
                    let mut bytes_to_show = text_stops[stop_idx];
                    if let Scene::LineAppearing(..) = self.scene {
                        const CPS: f32 = 30.;
                        let bytes_shown_before = if stop_idx == 0 {
                            0
                        } else {
                            text_stops[stop_idx - 1]
                        };
                        let chars_to_show =
                            ((CPS * scene_duration.as_secs_f32()).trunc()).max(1.) as usize;
                        bytes_to_show = bytes_shown_before
                            + &text[bytes_shown_before..]
                                .char_indices()
                                .nth(chars_to_show - 1)
                                .unwrap_or((text_stops[stop_idx] - bytes_shown_before, '\0'))
                                .0;
                        line_just_stopped = bytes_to_show == text_stops[stop_idx];
                    }
                    graphics.draw_image(panel_pos, panel);
                    let block = self.font.layout_text(
                        &text[0..bytes_to_show],
                        20.,
                        TextOptions::new()
                            .with_wrap_to_width(panel.size().x as f32 * 0.9, TextAlignment::Left),
                    );
                    let text_pos = (panel_pos.0 + 20., panel_pos.1 + 20.);
                    graphics.draw_text(text_pos, Color::YELLOW, &block);
                }
                if line_just_stopped {
                    self.next_scene();
                }
            }
            #[allow(unreachable_patterns)]
            _ => todo!("`on_draw` does not handle {:?}", self.scene),
        }
        helper.request_redraw();
    }

    fn on_key_down(&mut self, _helper: &mut WindowHelper, key: Option<KeyCode>, _scan: KeyScan) {
        if let Some(key) = key {
            match (self.scene, &key) {
                (
                    Scene::SplashScreen | Scene::LineAppearing(..) | Scene::LineStatic(..),
                    KeyCode::Return | KeyCode::Right | KeyCode::Space | KeyCode::L,
                ) => {
                    self.next_scene();
                }
                (Scene::LineAppearing(..) | Scene::LineStatic(..), KeyCode::Left | KeyCode::H) => {
                    self.prev_scene();
                }
                _ => {}
            }
        }
    }

    fn on_mouse_move(&mut self, _helper: &mut WindowHelper, position: Vector2<f32>) {
        self.mouse_x = position.x;
        self.mouse_y = position.y;
        //helper.request_redraw();
    }

    fn on_mouse_button_down(&mut self, _helper: &mut WindowHelper, button: MouseButton) {
        match (self.scene, &button) {
            (Scene::SplashScreen, _) => self.next_scene(),
            (Scene::MainMenu(Some(0)), MouseButton::Left) => {
                self.switch_scene(Scene::ChapterIntro(0));
            }
            _ => {}
        }
    }

    fn on_resize(&mut self, _helper: &mut WindowHelper, size: Vector2<u32>) {
        self.window_w = size.x;
        self.window_h = size.y;
        //helper.request_redraw();
    }
}
