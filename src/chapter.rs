use std::{fs::File, io::Read};

/// A sequence of [`Paragraph`]s.
pub struct Chapter {
    pub paragraphs: Vec<Paragraph>,
}
impl From<File> for Chapter {
    fn from(mut f: File) -> Self {
        let mut contents = String::new();
        f.read_to_string(&mut contents).unwrap();
        let paragraphs = contents.split("\n\n");
        Self {
            paragraphs: paragraphs.map(|x| Paragraph::from(x)).collect(),
        }
    }
}

/// A sequence of [`Line`]s with a common backround.
pub struct Paragraph {
    pub bg_filename: String,
    pub lines: Vec<Line>,
}
impl From<&str> for Paragraph {
    fn from(p: &str) -> Self {
        let mut lines = p.lines();
        Self {
            bg_filename: lines.next().unwrap().to_owned(),
            lines: lines.map(|x| Line::from(x)).collect(),
        }
    }
}

/// A single bit of optional text and optional sound.
pub struct Line {
    //sound_filename: Option<String>,
    pub text: Option<String>,
    pub text_stops: Vec<usize>,
}
impl From<&str> for Line {
    fn from(l: &str) -> Self {
        let mut text = l.to_owned();
        let mut text_stops = vec![];
        while let Some(idx) = text.find("\\s") {
            text_stops.push(idx);
            text = text.replacen("\\s", "", 1);
        }
        text_stops.push(text.len());
        Self {
            //sound_filename: None,
            text: Some(text),
            text_stops,
        }
    }
}
