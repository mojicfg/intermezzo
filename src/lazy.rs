use speedy2d::{
    image::{ImageHandle, ImageSmoothingMode},
    Graphics2D,
};

pub enum LazyResource<T> {
    Available(T),
    Filename(String),
    Vacant,
}
impl<T> LazyResource<T> {
    pub fn unwrap(&self) -> &T {
        match self {
            LazyResource::Available(x) => x,
            _ => panic!("called `LazyResource::unwrap()` on a non-available value"),
        }
    }
}
pub type LazyImage = LazyResource<ImageHandle>;
impl LazyImage {
    pub fn load_if_filename(&mut self, graphics: &mut Graphics2D) {
        if let LazyImage::Filename(filename) = &self {
            let path = format!("assets/{}", filename);
            let bg = graphics
                .create_image_from_file_path(None, ImageSmoothingMode::Linear, path)
                .unwrap();
            *self = LazyImage::Available(bg);
        }
    }
}
